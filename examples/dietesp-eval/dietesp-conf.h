#ifndef __DIETESP_CONF_H__
#define __DIETESP_CONF_H__

/* include definitions and common settings */
#include "dietesp-conf-pre.h"

/*
 * DEBUG activated (1) or not (0)
 * DEBUG will activate the console and therefore increase power usage
 */
#define APP_DEBUG                       0

/*
 * radio channel
 */
#define RF2XX_CHANNEL                   26

/*
 * Mode of communications.
 * DATA_CLEAR, DATA_IPSEC, DATA_DIET_PARTIAL, DATA_DIET_FULL, DATA_DIET_MANUAL
 */
#define COMS_PROFILE                    DATA_DIET_FULL

/*
 * Diet-ESP profile when COMS_PROFILE is DATA_DIET_MANUAL
 */
#define MANUAL_DIETESP_SPI_SIZE         0
#define MANUAL_DIETESP_SN_SIZE          0
#define MANUAL_DIETESP_NH               1
#define MANUAL_DIETESP_PAD              1
#define MANUAL_DIETESP_ALIGN            0
#define MANUAL_DIETESP_TH               1
#define MANUAL_DIETESP_IH               0
#define MANUAL_DIETESP_ICV              1
#define MANUAL_DIETESP_IV               1

/*
 * addresses and ports to use for testing (useful to help control mote identify itself)
 */
#define BROADCAST      "\xff\x02\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x02"
#define STRAS_M3_45    "\xfe\x80\x00\x00\x00\x00\x00\x00\x03\x23\x45\x01\x33\x85\x03\x43"
#define STRAS_M3_46    "\xfe\x80\x00\x00\x00\x00\x00\x00\x03\x23\x45\x01\x19\x85\x03\x43"
#define GRENOBLE_M3_11 "\xfe\x80\x00\x00\x00\x00\x00\x00\x03\x23\x45\x01\x19\x71\x03\x43"
#define GRENOBLE_M3_13 "\xfe\x80\x00\x00\x00\x00\x00\x00\x03\x23\x45\x01\x25\x69\x03\x43"
#define LILLE_M3_36    "\xfe\x80\x00\x00\x00\x00\x00\x00\x03\x23\x45\x01\x31\x89\x03\x43"
#define LILLE_M3_37    "\xfe\x80\x00\x00\x00\x00\x00\x00\x03\x23\x45\x01\x24\x89\x03\x43"
#define LILLE_M3_38    "\xfe\x80\x00\x00\x00\x00\x00\x00\x03\x23\x45\x01\x21\x51\x02\x43"
#define LILLE_M3_39    "\xfe\x80\x00\x00\x00\x00\x00\x00\x03\x23\x45\x01\x32\x71\x03\x43"

/*
 * The control mote and UUT share the same software
 * but behave differently
 * Each mote detects its role on boot
 */
#define CONTROL_MOTE                    GRENOBLE_M3_11
#define UUT_MOTE                        GRENOBLE_M3_13

/*
 * Select the experiment here
 * CONSOLE or EXP_x, where x is 1 to 7
 */
#define TEST_MODE                       CONSOLE

/*
 * Specific experiment settings
 */
#define EXP_1_ITERATIONS                20
#define EXP_1_MIN_PAYLOAD               1
#define EXP_1_MAX_PAYLOAD               1200
#define EXP_1_PAYLOAD_STEP              1

#define EXP_2_ITERATIONS                20
#define EXP_2_MIN_PAYLOAD               1
#define EXP_2_MAX_PAYLOAD               1200
#define EXP_2_PAYLOAD_STEP              1

#define EXP_3_ITERATIONS                20
#define EXP_3_MIN_PAYLOAD               1
#define EXP_3_MAX_PAYLOAD               1200
#define EXP_3_PAYLOAD_STEP              1

#define EXP_4_ITERATIONS                20
#define EXP_4_MIN_PAYLOAD               1
#define EXP_4_MAX_PAYLOAD               1200
#define EXP_4_PAYLOAD_STEP              1

#define EXP_5_ITERATIONS                20
#define EXP_5_MIN_PAYLOAD               1
#define EXP_5_MAX_PAYLOAD               1200
#define EXP_5_PAYLOAD_STEP              1

#define EXP_6_ITERATIONS                20
#define EXP_6_SLEEP_TIME                10
#define EXP_6_PAYLOAD                   10 /* 200 */

#define EXP_7_ITERATIONS                50
#define EXP_7_MIN_PAYLOAD               1
#define EXP_7_MAX_PAYLOAD               1200
#define EXP_7_PAYLOAD_STEP              15
#define EXP_7_SLEEP_TIME                10

/*
 * Manual SA configuration allow the developer to
 * create persistent SAs in the SAD.
 * This is probably what you want to use if
 * WITH_CONF_IPSEC_IKE is set 0, but please note
 * that both features can be used simultaneously
 * on a host as per the IPsec RFC.
 *
 * The manual SAs can be set in the function sad_conf()
 */
#define WITH_CONF_MANUAL_SA             1

/*
 * The IKE subsystem is optional if the SAs are manually configured
 */
#define WITH_CONF_IPSEC_IKE             0



/* include definitions and common settings deduced
 * from choices in this file */
#include "dietesp-conf-post.h"

#endif // __DIETESP_CONF_H__

