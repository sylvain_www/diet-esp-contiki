Diet-ESP evalution code
=======================
This contiki example uses a modified IPSEC stack that supports the Diet-ESP protocol.

The aim of the application is to measure on a mote the savings that can be achieved when using Diet-ESP to secure communications, compared to standard ESP.
