/**
 * \file
 *         Test application for Diet-ESP protocol
 *         Designed for INRIA's Iot-lab, M3 nodes
 * \author
 *         Sylvain Killian (sylvain.www+devel@gmail.com)
 */

#include "contiki.h"
#ifdef IOTLAB
#include "platform.h"
#include "stm32f1xx/pwr.h"
#endif
#include <stdio.h>
#include "uip.h"
#include "uip-ds6.h"
#include "uip-udp-packet.h"
#include "common_ipsec.h"
#include "sad.h"
#ifdef IOTLAB_M3
#include "lps331ap.h"
#include "isl29020.h"
#include "lsm303dlhc.h"
#include "l3g4200d.h"
#endif
#include "rime.h"

#if APP_DEBUG
void memprint(uint8_t *ptr, const uint16_t len);
#define APP_PRINTF(format, ...)         printf("\napp  : " format "\n", ## __VA_ARGS__)
#define APP_MEMPRINTF(str, ptr, len)    do {                                    \
                                            printf("\napp  : " str  " (len %u):\n", len);   \
                                            memprint((uint8_t*)ptr, len);       \
                                        } while(0)
#else // APP_DEBUG
#define APP_PRINTF(...)
#define APP_MEMPRINTF(...)
#endif // APP_DEBUG

#if TEST_CODE != TEST_CONSOLE
#define CONSOLE_PRINTF(...)
#define CONSOLE_NONL(...)
#define APP_PRINT6ADDR(...)
#else // TEST_CODE
#define CONSOLE_PRINTF(format, ...)     printf(format "\n", ## __VA_ARGS__)
#ifdef IOTLAB
#define CONSOLE_NONL(format, ...)       do {\
                                            printf(format , ## __VA_ARGS__);    \
                                            fflush(stdout);                     \
                                        } while (0)
#else
#define CONSOLE_NONL(...)               APP_PRINTF(__VA_ARGS__)
#endif
#define APP_PRINT6ADDR(addr)            printf("%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x\n", \
                                        ((uint8_t *)addr)[0], ((uint8_t *)addr)[1],    \
                                        ((uint8_t *)addr)[2], ((uint8_t *)addr)[3],    \
                                        ((uint8_t *)addr)[4], ((uint8_t *)addr)[5],    \
                                        ((uint8_t *)addr)[6], ((uint8_t *)addr)[7],    \
                                        ((uint8_t *)addr)[8], ((uint8_t *)addr)[9],    \
                                        ((uint8_t *)addr)[10], ((uint8_t *)addr)[11],  \
                                        ((uint8_t *)addr)[12], ((uint8_t *)addr)[13],  \
                                        ((uint8_t *)addr)[14], ((uint8_t *)addr)[15])
#endif // TEST_CODE

#define CMD_END                         1

#ifdef IOTLAB_M3
// marker: use LEDs to create an inverted current peak
#define MARKER_ON() leds_off(LED_0 | LED_1 | LED_2)
#define MARKER_OFF() leds_on(LED_0 | LED_1 | LED_2)
#else
#define MARKER_ON()
#define MARKER_OFF()
#endif
#define HIGH() leds_on(LED_0 | LED_1 | LED_2)
#define LOW() leds_off(LED_0 | LED_1 | LED_2)

/* a 1000 bytes array to send stuff */
static const uint8_t DATA[] = "123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890";

static void reset_sequence_numbers(void);
static int  handle_cmd(uint8_t arg);
static void handle_delayed_cmd(uint8_t arg);
#if APP_PERIPHERALS
static void temperature_sensor(void);
static void light_sensor(void);
static void pressure_sensor(void);
#endif
static void print_local_address(void);
static void print_usage(void);
static void marker_pulse_init(void);
static void marker_pulse_start(int duration);
static void marker_pulse_continue(int duration);
static void marker_pulse_end(void);
static void uart_init(void);
static void light_sensor_init(void);
static void pressure_sensor_init(void);
static void temperature_sensor_init(void);
static void gyro_sensor_init(void);
static void accel_sensor_init(void);
#if TEST_CODE == TEST_CONSOLE
static void char_rx(handler_arg_t arg, uint8_t c);
static void prompt(void);
#endif
static void send_packet(uint8_t *data, uint16_t datalen);
static char packet_handler(void);

PROCESS(diet_esp_process, "Diet-ESP Example");
AUTOSTART_PROCESSES(&diet_esp_process);

static struct uip_udp_conn *inbound_conn;
static struct uip_udp_conn *outbound_conn;
static int i_am_the_controller = false;

CCIF process_event_t uart_event = NULL;
CCIF process_event_t radio_done = NULL;

enum {
    CONSUMMED,
    DELAYED
};

static void reset_sequence_numbers(void) {
    ipsec_addr_t packet_tag;
    uip_ip6addr_t peer;
    uip_str2ip6(i_am_the_controller? UUT_MOTE:CONTROL_MOTE, &peer);
    packet_tag.my_port = MOTE_PORT;
    packet_tag.peer_port = DEST_PORT;
    packet_tag.nextlayer_proto = UIP_PROTO_UDP;
    packet_tag.peer_addr = &peer;
    sad_entry_t *sad_entry = sad_get_outgoing_entry(&packet_tag);
    if (!sad_entry) {
        IPSEC_PRINTF_ERROR("failed to reset outgoing SAD");
    } else {
        sad_entry->seqno = 0;
    }
    sad_entry = sad_get_incoming_entry(i_am_the_controller? SPI_CONTROL_IN:SPI_UUT_IN);
    if (!sad_entry) {
        IPSEC_PRINTF_ERROR("failed to reset incoming SAD");
    } else {
        sad_entry->seqno = 0;
    }
    CONSOLE_PRINTF("SAD packet sequence numbers have been reset to 0");
}

static int handle_cmd(uint8_t arg) {
    int i;
    switch (arg) {
    case 'a':
    {
        uip_ip6addr_t dest_host;
        CONSOLE_PRINTF("local : ");
        print_local_address();
        CONSOLE_PRINTF("");
        CONSOLE_PRINTF("remote: ");
        uip_str2ip6(i_am_the_controller? UUT_MOTE:CONTROL_MOTE, &dest_host);
        APP_PRINT6ADDR(&dest_host);
        CONSOLE_PRINTF("");
        return CONSUMMED;
    }
    case 'r':
        reset_sequence_numbers();
        return CONSUMMED;
#if APP_PERIPHERALS
    case 't':
        temperature_sensor();
        return CONSUMMED;
    case 'l':
        light_sensor();
        return CONSUMMED;
    case 'p':
        pressure_sensor();
        return CONSUMMED;
#endif
    case 'd':
    case 'D':
    case 'f':
    case 'F':
        if (i_am_the_controller) {
            send_packet(&arg, 1);
            return CONSUMMED;
        } else {
            return DELAYED;
        }
    case 'z':
        // delay command and frame it with a marker
        return DELAYED;
    }
    print_usage();
    return CONSUMMED;
}

static void handle_delayed_cmd(uint8_t arg) {
    switch (arg) {
    case 'z':
        CONSOLE_PRINTF("nop");
        break;
    case 'd':
        send_packet((uint8_t*)DATA, 1);
        break;
    case 'D':
        send_packet((uint8_t*)DATA, 28);
        break;
    case 'f':
        send_packet((uint8_t*)DATA, 5);
        break;
    case 'F':
        send_packet((uint8_t*)DATA, 400);
        break;
    }
}

static void print_usage(void) {
    CONSOLE_PRINTF("");;
    CONSOLE_PRINTF("h: print this help");
    CONSOLE_PRINTF("a: print addresses");
#if APP_PERIPHERALS
    CONSOLE_PRINTF("t: temperature measure");
    CONSOLE_PRINTF("l: luminosity measure");
    CONSOLE_PRINTF("p: pressure measure");
#endif
    CONSOLE_PRINTF("z: do nothing                   [with marker]");
    if (i_am_the_controller) {
        CONSOLE_PRINTF("d: tell to run command d (send dummy frame of size 1)   [with marker]");
        CONSOLE_PRINTF("D: tell to run command D (send dummy frame of size 28)  [with marker]");
        CONSOLE_PRINTF("f: tell to run command f (send dummy frame of size 5)   [with marker]");
        CONSOLE_PRINTF("F: tell to run command F (send dummy frame of size 400) [with marker]");
    } else {
        CONSOLE_PRINTF("d: send dummy frame of size 1   [with marker]");
        CONSOLE_PRINTF("D: send dummy frame of size 28  [with marker]");
        CONSOLE_PRINTF("f: send dummy frame of size 5   [with marker]");
        CONSOLE_PRINTF("F: send dummy frame of size 400 [with marker]");
    }
}

static void print_local_address(void) {
    int i;
    uint8_t state;

    for(i = 0; i < UIP_DS6_ADDR_NB; i++) {
        state = uip_ds6_if.addr_list[i].state;
        if(uip_ds6_if.addr_list[i].isused &&
               (state == ADDR_TENTATIVE || state == ADDR_PREFERRED)) {
            APP_PRINT6ADDR(&uip_ds6_if.addr_list[i].ipaddr);
        }
    }
}

#if APP_PERIPHERALS
static void temperature_sensor(void) {
#ifdef IOTLAB_M3
    int16_t value;
    lps331ap_read_temp(&value);
    CONSOLE_PRINTF("Chip temperature measure: %f", 42.5 + value / 480.0);
#else
    CONSOLE_PRINTF("not available");
#endif
}

static void light_sensor(void) {
#ifdef IOTLAB_M3
    float value = isl29020_read_sample();
    CONSOLE_PRINTF("Luminosity measure: %f lux", value);
#else
    CONSOLE_PRINTF("not available");
#endif
}

static void pressure_sensor(void) {
#ifdef IOTLAB_M3
    uint32_t value;
    lps331ap_read_pres(&value);
    CONSOLE_PRINTF("Pressure measure: %f mabar", value / 4096.0);
#else
    CONSOLE_PRINTF("not available");
#endif
}
#endif

static void marker_pulse_init(void) {
    MARKER_OFF();
}

static void marker_pulse_start(int duration) {
    static struct etimer timer;
    MARKER_ON();
    // Initialize a timer
    etimer_set(&timer, duration);
}

static void marker_pulse_continue(int duration) {
    static struct etimer timer;
    // Initialize a timer
    etimer_set(&timer, duration);
}

static void marker_pulse_end(void) {
    // end the marker by restoring LEDs to full power
    MARKER_OFF();
}

static void uart_init(void) {
    // Uart initialisation
#if TEST_CODE == TEST_CONSOLE
    uart_enable(uart_print, APP_UART_BAUDRATE);
    uart_event = process_alloc_event();
    uart_set_rx_handler(uart_print, char_rx, NULL);
#endif // TEST_CODE
}

static void light_sensor_init(void) {
#ifdef IOTLAB_M3
#if APP_PERIPHERALS
    // ISL29020 light sensor initialisation
    isl29020_prepare(ISL29020_LIGHT__AMBIENT, ISL29020_RESOLUTION__16bit,
                        ISL29020_RANGE__16000lux);
    isl29020_sample_continuous();
#else
    isl29020_powerdown();
#endif
#else
    APP_PRINTF("no light sensor implemented on this platform");
#endif
}

static void pressure_sensor_init(void) {
#ifdef IOTLAB_M3
#if APP_PERIPHERALS
    // LPS331AP pressure sensor initialisation
    lps331ap_powerdown();
    lps331ap_set_datarate(LPS331AP_P_12_5HZ_T_12_5HZ);
#else
    lps331ap_powerdown();
#endif
#else
    APP_PRINTF("no pressure sensor implemented on this platform");
#endif
}

static void temperature_sensor_init(void) {
#ifdef IOTLAB_M3
    // same as pressure sensor
#else
    APP_PRINTF("no temperature sensor implemented on this platform");
#endif
}

static void gyro_sensor_init(void) {
#ifdef IOTLAB_M3
#if !APP_PERIPHERALS
    l3g4200d_powerdown();
#else
#endif
#else
    APP_PRINTF("no gyro sensor implemented on this platform");
#endif
}

static void accel_sensor_init(void) {
#ifdef IOTLAB_M3
#if !APP_PERIPHERALS
    lsm303dlhc_powerdown();
#else
#endif
#else
    APP_PRINTF("no accelerometer sensor implemented on this platform");
#endif
}

/* Reception of a char on UART */
#if TEST_CODE == TEST_CONSOLE
static void char_rx(handler_arg_t arg, uint8_t c) {
    if (uart_event) {
        process_post(&diet_esp_process, uart_event, (void*)(uint32_t)c);
    }
}

static inline void prompt(void) {
    CONSOLE_NONL("\n> ");
}
#endif

/* Send data over UDP */
static void send_packet(uint8_t *data, uint16_t datalen) {
    APP_MEMPRINTF("Outgoing data", data, datalen);
    uip_udp_packet_send(outbound_conn, data, datalen);
}

/* Receive packet over UDP and accept command or reply ROT(1) */
static char packet_handler(void) {
    uint8_t *data = uip_appdata;
    uint16_t datalen = uip_datalen();

    APP_MEMPRINTF("Incoming data", data, datalen);

    memprint(data, datalen);
    printf("\n");
    // CONTROL NODE: do not reply and avoid entering an infinite ping pong game!
    if (!i_am_the_controller && uip_newdata()) {
        int i=0;
        if (datalen == 1) {
            /* consider 1 byte packets to be commands */
            return data[0];
        }

        /* longers packets: reply a ROT(1) */
        for(i = 0; i < datalen; i++) {
            ++data[i];
        }
        send_packet(data, datalen);
    }

#if TEST_CODE == TEST_CONSOLE
    prompt();
#endif
    return 0;
}

void set_prefix_64(void) {
    static uip_ip6addr_t ipaddr;
    uip_ip6addr(&ipaddr, 0xaaaa, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000);
    uip_ds6_set_addr_iid(&ipaddr, &uip_lladdr);
    uip_ds6_addr_add(&ipaddr, 0, ADDR_AUTOCONF);
}

#if TEST_CODE != TEST_CONSOLE
#define BKP_BASE_ADDRESS    0x40006C00
#define BKP_ITER            0x04            /* test iteration counter */
#define BKP_FRAMES          0x08            /* number of radio frames for this iteration */
#define BKP_PAYLOAD         0x0C            /* payload len */

inline static volatile uint32_t *rtc_backup_reg(int offset) {
    return mem_get_reg32(BKP_BASE_ADDRESS + offset);
}

static void input_printsniffer(void) { }
static void output_printsniffer(int mac_status) {
    static int counter = 0;
    if (*rtc_backup_reg(BKP_ITER) == 0) {
        // first test: count number of radio frames required
        *rtc_backup_reg(BKP_FRAMES) = *rtc_backup_reg(BKP_FRAMES) + 1;
        //printf("init: one more frame\n");
    } else {
        //printf("counter: %lx / %lx\n", counter, *rtc_backup_reg(BKP_FRAMES));
        if (++counter >= *rtc_backup_reg(BKP_FRAMES)) {
            //printf("all frames sent, sleep\n");
            // frame was sent entirely, return to low power mode
            counter = 0;
            *rtc_backup_reg(BKP_ITER) = *rtc_backup_reg(BKP_ITER) + 1;
#if TEST_CODE == TEST_CODE_1
            if (radio_done) {
                process_post(&diet_esp_process, radio_done, NULL);
            }
#elif TEST_CODE == TEST_CODE_2
            if (*rtc_backup_reg(BKP_ITER) > ITERATIONS) {
                *rtc_backup_reg(BKP_ITER) = 0;
            }
            pwr_enter_standby();
#endif // TEST_CODE
        }
    }
}
RIME_SNIFFER(printsniff, input_printsniffer, output_printsniffer);
#endif // TEST_CODE

/* Contiki process to init app and monitor network activity */
PROCESS_THREAD(diet_esp_process, ev, data) {
    PROCESS_BEGIN();

    uint8_t i;
    uip_ip6addr_t dest_host;
    static uint8_t cmd = 0;

    uart_init();
    light_sensor_init();
    pressure_sensor_init();
    temperature_sensor_init();
    gyro_sensor_init();
    accel_sensor_init();

    print_local_address();

    i_am_the_controller = is_my_address(CONTROL_MOTE);
    uip_str2ip6(i_am_the_controller? UUT_MOTE:CONTROL_MOTE, &dest_host);
    uip_ds6_nbr_add(&dest_host, NULL, 0, NBR_INCOMPLETE);

    /* UDP connections with remote host */
    inbound_conn = udp_new(NULL, UIP_HTONS(0), NULL);
    udp_bind(inbound_conn, UIP_HTONS(MOTE_PORT));
    CONSOLE_PRINTF("I'm listening on UDP port %d.\n\n", MOTE_PORT);

    outbound_conn = udp_new(&dest_host, UIP_HTONS(DEST_PORT), NULL);
    udp_bind(outbound_conn, UIP_HTONS(MOTE_PORT));

#if TEST_CODE == TEST_CODE_1
    // energy vs payload; send in a loop for a range of payloads
    static int iter = 0;
    static struct etimer timer;
    rime_sniffer_add(&printsniff);
    if (i_am_the_controller) {

        while (1) {
            PROCESS_YIELD();

            if (ev == tcpip_event) {
                // Frame received
                packet_handler();
                reset_sequence_numbers();
                printf("received frame %d\n", iter++);
            }
        }

    } else {
        enum { TEST_SYNC_START, TEST_SYNC, TEST_SYNC_END, TEST_START, TEST_RUN, TEST_END };
        static int state = TEST_SYNC_START;
        static int sync;
        static uint16_t payload_len = MIN_PAYLOAD;
        // Initialize a timer
        etimer_set(&timer, START_WAIT_TIME);
        marker_pulse_init();

        *rtc_backup_reg(BKP_FRAMES) = 0;
        // Initialize a timer
        radio_done = process_alloc_event();

        while (1) {
            PROCESS_YIELD();

            if (ev == PROCESS_EVENT_TIMER) {
                switch (state) {
                case TEST_SYNC_START:
                    printf("testing with payload: %d bytes\n", payload_len);
                    sync = 16; // number of bits coded in the power graph
                    state = TEST_SYNC;
                    etimer_set(&timer, SYNC_START);
                    break;
                case TEST_SYNC:
                    sync--;
                    if ((payload_len >> sync) & 1) {
                        HIGH();
                    } else {
                        LOW();
                    }
                    etimer_set(&timer, BIT_LEN);
                    if (!sync) {
                        state = TEST_SYNC_END;
                    }
                    break;
                case TEST_SYNC_END:
                    /* first test: send packet and let ample time pass
                     * so that we can determine in the callback the
                     * number of frames required to transmit that
                     * payload */
                    HIGH();
                    state = TEST_START;
                    *rtc_backup_reg(BKP_ITER) = 0;
                    *rtc_backup_reg(BKP_FRAMES) = 0;
                    send_packet((uint8_t*)DATA, payload_len);
                    etimer_set(&timer, MAX_PACKET_SEND + SYNC_START);
                    break;
                case TEST_START:
                    state = TEST_RUN;
                    LOW();
                    *rtc_backup_reg(BKP_ITER) = *rtc_backup_reg(BKP_ITER) + 1;
                    send_packet((uint8_t*)DATA, payload_len);
                    reset_sequence_numbers();
                    break;
                case TEST_END:
                    if (payload_len > MAX_PAYLOAD) {
                        printf("test is complete\n");
                        etimer_set(&timer, CLOCK_SECOND*5);
                    } else {
                        state = TEST_SYNC_START;
                        etimer_set(&timer, CLOCK_SECOND);
                    }
                    break;
                }
            }

            if (ev == radio_done && state == TEST_RUN) {
                if (*rtc_backup_reg(BKP_ITER) > ITERATIONS) {
                    HIGH();
                    payload_len += PAYLOAD_STEP;
                    state = TEST_END;
                    etimer_set(&timer, CLOCK_SECOND);
                } else {
                    send_packet((uint8_t*)DATA, payload_len);
                    reset_sequence_numbers();
                }
            }
        }

    }
#elif TEST_CODE == TEST_CODE_2
    // sleep, wake up and send PAYLOAD bytes of data in a loop
    static int iter = 0;
    static struct etimer timer;
    rime_sniffer_add(&printsniff);
    if (i_am_the_controller) {

        while (1) {
            PROCESS_YIELD();

            if (ev == tcpip_event) {
                // Frame received
                packet_handler();
                reset_sequence_numbers();
                printf("received frame %d\n", iter++);
            }
        }

    } else {
        //printf("test number: %lx\n", *rtc_backup_reg(BKP_ITER));
        if (*rtc_backup_reg(BKP_ITER) == 0) {
            /* first test: send packet and let ample time pass
             * so that we can determine in the callback the
             * number of frames required to transmit that
             * payload */
            *rtc_backup_reg(BKP_FRAMES) = 0;

            send_packet((uint8_t*)DATA, PAYLOAD);
            etimer_set(&timer, MAX_PACKET_SEND + SYNC_START);

            while (1) {
                PROCESS_YIELD();

                if (ev == PROCESS_EVENT_TIMER) {
                    *rtc_backup_reg(BKP_ITER) = *rtc_backup_reg(BKP_ITER) + 1;
                    rtc_set_alarm(rtc_time()+SLEEP_TIME, NULL, NULL);
                    pwr_enter_standby();
                    break;
                }
            }
        } else {
            /* test ongoing, setup alarm for the next wake-up */
            rtc_set_alarm(rtc_time()+SLEEP_TIME, NULL, NULL);
            send_packet((uint8_t*)DATA, PAYLOAD);

            /* setup a timeout in case the test gets stuck
               it happens sometimes, not sure why (probably less
               radio frames than expected */
            etimer_set(&timer, (CLOCK_SECOND*5));

            while (1) {
                PROCESS_YIELD();

                if (ev == PROCESS_EVENT_TIMER) {
                    printf("timeout!");
                    rtc_set_alarm(rtc_time()+SLEEP_TIME, NULL, NULL);
                    pwr_enter_standby();
                }
            }
        }
    }
#elif TEST_CODE == TEST_CODE_3
    // send and sleep for range of payloads
    static int iter = 0;
    static struct etimer timer;
    rime_sniffer_add(&printsniff);
    if (i_am_the_controller) {

        while (1) {
            PROCESS_YIELD();

            if (ev == tcpip_event) {
                // Frame received
                packet_handler();
                reset_sequence_numbers();
                printf("received frame %d\n", iter++);
            }
        }

    } else {
        //printf("test number: %lx\n", *rtc_backup_reg(BKP_ITER));
        if (*rtc_backup_reg(BKP_ITER) == 0) {
            /* first test: send packet and let ample time pass
             * so that we can determine in the callback the
             * number of frames required to transmit that
             * payload */
            enum { TEST_SYNC_START, TEST_SYNC, TEST_SYNC_END, TEST_START };
            static int state = TEST_SYNC_START;
            static int sync;
            static uint16_t payload_len = 0;
            payload_len = *rtc_backup_reg(BKP_PAYLOAD);
            payload_len = (payload_len == 0) ? MIN_PAYLOAD: payload_len + PAYLOAD_STEP;

            if (payload_len > MAX_PAYLOAD) {
                // end of test
                pwr_enter_standby();
            }

            *rtc_backup_reg(BKP_PAYLOAD) = payload_len;
            *rtc_backup_reg(BKP_FRAMES) = 0;

            // Initialize a timer
            etimer_set(&timer, (CLOCK_SECOND*2));
            marker_pulse_init();

            while (1) {
                PROCESS_YIELD();

                if (ev == PROCESS_EVENT_TIMER) {
                    switch (state) {
                    case TEST_SYNC_START:
                        printf("testing with payload: %d bytes\n", payload_len);
                        sync = 16; // number of bits coded in the power graph
                        state = TEST_SYNC;
                        etimer_set(&timer, SYNC_START);
                        break;
                    case TEST_SYNC:
                        sync--;
                        if ((payload_len >> sync) & 1) {
                            HIGH();
                        } else {
                            LOW();
                        }
                        etimer_set(&timer, BIT_LEN);
                        if (!sync) {
                            state = TEST_SYNC_END;
                        }
                        break;
                    case TEST_SYNC_END:
                        HIGH();
                        state = TEST_START;
                        send_packet((uint8_t*)DATA, payload_len);
                        etimer_set(&timer, MAX_PACKET_SEND + SYNC_START);
                        break;
                    case TEST_START:
                        *rtc_backup_reg(BKP_ITER) = *rtc_backup_reg(BKP_ITER) + 1;
                        rtc_set_alarm(rtc_time()+SLEEP_TIME, NULL, NULL);
                        pwr_enter_standby();
                        break;
                    }
                }
            }
        } else {
            /* test ongoing, setup alarm for the next wake-up */
            rtc_set_alarm(rtc_time()+SLEEP_TIME, NULL, NULL);
            send_packet((uint8_t*)DATA, *rtc_backup_reg(BKP_PAYLOAD));

            /* setup a timeout in case the test gets stuck
               it happens sometimes, not sure why (probably less
               radio frames than expected */
            etimer_set(&timer, (CLOCK_SECOND*5));
            marker_pulse_init();

            while (1) {
                PROCESS_YIELD();

                if (ev == PROCESS_EVENT_TIMER) {
                    printf("timeout with payload: %d bytes\n", *rtc_backup_reg(BKP_PAYLOAD));
                    rtc_set_alarm(rtc_time()+SLEEP_TIME, NULL, NULL);
                    pwr_enter_standby();
                }
            }
        }
    }
#else
    // console
    marker_pulse_init();
    print_usage();
    prompt();

    while (1) {
        PROCESS_YIELD();

        if (ev == tcpip_event) {
            // Frame received
            if ((cmd = packet_handler())) {
                if (handle_cmd(cmd) == DELAYED) {
                    marker_pulse_start(CLOCK_SECOND);
                } else {
                    cmd = 0;
                    prompt();
                }
            }
        }

        else if (ev == uart_event) {
            // UART char received
            //  Chars are received one by one,
            //  with a \n at the end
            if ((char)(uint32_t)data != '\n') {
                // save char to use later, when \n is received
                // as  a by-product, only last char of input line is considered
                cmd = (char)(uint32_t)data;
                continue;
            }

            // received \n, cmd contains last char
            if (handle_cmd(cmd) == DELAYED) {
                marker_pulse_start(CLOCK_SECOND);
                // keep cmd for use when marker expires
            } else {
                cmd = 0;
                prompt();
            }
        }

        else if (ev == PROCESS_EVENT_TIMER) {
            // End of marker period
            if (cmd != CMD_END) {
                //leds_on(LED_0 | LED_1 | LED_2);
                handle_delayed_cmd(cmd);
                // second half of the marker
                cmd = CMD_END;
                marker_pulse_continue(CLOCK_SECOND);
            } else {
                // end of marker pulse
                marker_pulse_end();
                cmd = 0;
                prompt();
            }
        }
    }

#endif // autotest / console

    PROCESS_END();
}

