#ifndef __DIETESP_DEFS_PRE_H__
#define __DIETESP_DEFS_PRE_H__

/*
 * Configures the contiki code and diet-esp evaluation code,
 * but those values do not need to be changed by the developper
 * (definitions and common settings).
 * see dietesp-conf.h for configuration values that could be
 * tuned
 */

/*
 * The example is coded for IPV6 only, most common in IoT
 */
#define WITH_IPV6               1
#define UIP_CONF_IPV6           1

/*
 * Extra uIP logging
 */
#undef UIP_CONF_LOGGING
#define UIP_CONF_LOGGING        0

/*
 * caution: UIP_CONF_BUFFER_SIZE can be set in
 * various platform specific header files as well
 * The ipsec test code is NOT protected against
 * buffer overflows
 */
#ifdef UIP_CONF_BUFFER_SIZE
#undef UIP_CONF_BUFFER_SIZE
#endif
#define UIP_CONF_BUFFER_SIZE    1200

#ifdef SICSLOWPAN_CONF_FRAG
#undef SICSLOWPAN_CONF_FRAG
#endif
#define SICSLOWPAN_CONF_FRAG    1

/*
 * have enough slots in queue to transmit UIP_CONF_BUFFER_SIZE / MAX_PAYLOAD
 * bytes in radio frames of size 125 bytes (counting external headers)
 */
#define QUEUEBUF_CONF_NUM       20

/*
 * Configuring an AES implementation
 *
 * The only current implementation is the one provided
 * by the MIRACL -library. In the future this can be
 * extended with an interface to the CC2420 radio
 * module which is equipped with an AES hardware implementation.
 */
#define CRYPTO_CONF_AES                 miracl_aes /* cc2420_aes */

#define MOTE_PORT                       1234
#define DEST_PORT                       1234

/*
 * communication profile definitions
 */
#define DATA_CLEAR                      0
#define DATA_IPSEC                      1
#define DATA_DIET_PARTIAL               2
#define DATA_DIET_FULL                  3
#define DATA_DIET_MANUAL                4

/*
 * experiments definitions
 */
#define CONSOLE                         0
#define EXP_1                           1
#define EXP_2                           2
#define EXP_3                           3
#define EXP_4                           4
#define EXP_5                           5
#define EXP_6                           6
#define EXP_7                           7

/*
 * the various test codes
 */
#define TEST_CONSOLE                    0
#define TEST_CODE_1                     1
#define TEST_CODE_2                     2
#define TEST_CODE_3                     3

/*
 * experiments definitions
 */

/*
 * UART console activated (1) or not (0)
 * Deactivate the console/uart will reduce global current consumption
 */
#define APP_UART_BAUDRATE               500000

/*
 * Hardcoded configuration of SPIs
 * inverted for control mote and UUT
 */
#define SPI_CONTROL_OUT                 1
#define SPI_CONTROL_IN                  2
#define SPI_UUT_OUT                     2
#define SPI_UUT_IN                      1

/*
 * other common settings
 */
#define START_WAIT_TIME                 (CLOCK_SECOND*15)
#define SYNC_START                      (CLOCK_SECOND*4)
#define MAX_PACKET_SEND                 (CLOCK_SECOND*5)
#define BIT_LEN                         (CLOCK_SECOND/5)

#endif // __DIETESP_DEFS_PRE_H__

