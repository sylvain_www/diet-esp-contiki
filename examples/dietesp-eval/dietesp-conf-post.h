#ifndef __DIETESP_DEFS_POST_H__
#define __DIETESP_DEFS_POST_H__

/*
 * IPsec configuration
 * Enabling ESP is equivalent to enabling IPsec
 * Note that AH is not supported!
 */
#if COMS_PROFILE == DATA_CLEAR
#define WITH_CONF_IPSEC_ESP             0
#else
#define WITH_CONF_IPSEC_ESP             1
#endif

/*
 * Configuration of diet-esp profile
 */
#if (COMS_PROFILE == DATA_IPSEC) || (COMS_PROFILE == CLEAR)
#define DIETESP_SPI_SIZE                4
#define DIETESP_SN_SIZE                 4
#define DIETESP_NH                      0
#define DIETESP_PAD                     0
#define DIETESP_ALIGN                   2
#define DIETESP_TH                      0
#define DIETESP_IH                      0
#define DIETESP_ICV                     0
#define DIETESP_IV                      0
#elif COMS_PROFILE == DATA_DIET_PARTIAL
#define DIETESP_SPI_SIZE                0
#define DIETESP_SN_SIZE                 0
#define DIETESP_NH                      1
#define DIETESP_PAD                     1
#define DIETESP_ALIGN                   0
#define DIETESP_TH                      1
#define DIETESP_IH                      0
#define DIETESP_ICV                     0
#define DIETESP_IV                      1
#elif COMS_PROFILE == DATA_DIET_FULL
#define DIETESP_SPI_SIZE                0
#define DIETESP_SN_SIZE                 0
#define DIETESP_NH                      1
#define DIETESP_PAD                     1
#define DIETESP_ALIGN                   0
#define DIETESP_TH                      1
#define DIETESP_IH                      0
#define DIETESP_ICV                     1
#define DIETESP_IV                      1
#elif COMS_PROFILE == DATA_DIET_MANUAL
#define DIETESP_SPI_SIZE                MANUAL_DIETESP_SPI_SIZE
#define DIETESP_SN_SIZE                 MANUAL_DIETESP_SN_SIZE
#define DIETESP_NH                      MANUAL_DIETESP_NH
#define DIETESP_PAD                     MANUAL_DIETESP_PAD
#define DIETESP_ALIGN                   MANUAL_DIETESP_ALIGN
#define DIETESP_TH                      MANUAL_DIETESP_TH
#define DIETESP_IH                      MANUAL_DIETESP_IH
#define DIETESP_ICV                     MANUAL_DIETESP_ICV
#define DIETESP_IV                      MANUAL_DIETESP_IV
#endif

#if TEST_MODE == EXP_1
#define TEST_CODE                       TEST_CODE_1
#define ITERATIONS                      EXP_1_ITERATIONS
#define MIN_PAYLOAD                     EXP_1_MIN_PAYLOAD
#define MAX_PAYLOAD                     EXP_1_MAX_PAYLOAD
#define PAYLOAD_STEP                    EXP_1_PAYLOAD_STEP

#elif TEST_MODE == EXP_2
#define TEST_CODE                       TEST_CODE_1
#define ITERATIONS                      EXP_2_ITERATIONS
#define MIN_PAYLOAD                     EXP_2_MIN_PAYLOAD
#define MAX_PAYLOAD                     EXP_2_MAX_PAYLOAD
#define PAYLOAD_STEP                    EXP_2_PAYLOAD_STEP

#elif TEST_MODE == EXP_3
#define TEST_CODE                       TEST_CODE_1
#define ITERATIONS                      EXP_3_ITERATIONS
#define MIN_PAYLOAD                     EXP_3_MIN_PAYLOAD
#define MAX_PAYLOAD                     EXP_3_MAX_PAYLOAD
#define PAYLOAD_STEP                    EXP_3_PAYLOAD_STEP

#elif TEST_MODE == EXP_4
#define TEST_CODE                       TEST_CODE_1
#define ITERATIONS                      EXP_4_ITERATIONS
#define MIN_PAYLOAD                     EXP_4_MIN_PAYLOAD
#define MAX_PAYLOAD                     EXP_4_MAX_PAYLOAD
#define PAYLOAD_STEP                    EXP_4_PAYLOAD_STEP

#elif TEST_MODE == EXP_5
#define TEST_CODE                       TEST_CODE_1
#define ITERATIONS                      EXP_5_ITERATIONS
#define MIN_PAYLOAD                     EXP_5_MIN_PAYLOAD
#define MAX_PAYLOAD                     EXP_5_MAX_PAYLOAD
#define PAYLOAD_STEP                    EXP_5_PAYLOAD_STEP

#elif TEST_MODE == EXP_6
#define TEST_CODE                       TEST_CODE_2
#define ITERATIONS                      EXP_6_ITERATIONS
#define PAYLOAD                         EXP_6_PAYLOAD
#define SLEEP_TIME                      EXP_6_SLEEP_TIME

#elif TEST_MODE == EXP_7
#define TEST_CODE                       TEST_CODE_3
#define ITERATIONS                      EXP_7_ITERATIONS
#define MIN_PAYLOAD                     EXP_7_MIN_PAYLOAD
#define MAX_PAYLOAD                     EXP_7_MAX_PAYLOAD
#define PAYLOAD_STEP                    EXP_7_PAYLOAD_STEP
#define SLEEP_TIME                      EXP_7_SLEEP_TIME

#else /* console mode */
#define TEST_CODE                       TEST_CONSOLE
#endif

/*
 * Sensors/peripherals activated (1) or not (0)
 * Deactivate the sensors will reduce global current consumption
 */
#if TEST_MODE == CONSOLE_MODE
#define APP_PERIPHERALS  1
#else
#define APP_PERIPHERALS  0
#endif

#endif // __DIETESP_DEFS_POST_H__
