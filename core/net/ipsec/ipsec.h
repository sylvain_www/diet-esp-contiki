/**
 * \addtogroup ipsec
 * @{
 */

/**
 * \file
 *        IPsec and IKEv2 configuration
 * \author
 *        Simon Duquennoy <simonduq@sics.se>
 *				Vilhelm Jutvik <ville@imorgon.se>
 */

#ifndef __IPSEC_H__
#define __IPSEC_H__

#include <contiki-conf.h>
#include "net/uip.h"
#include "uip-ds6.h"

#if WITH_CONF_IPSEC_AH
#define WITH_IPSEC_AH     WITH_CONF_IPSEC_AH
#else
#define WITH_IPSEC_AH     0
#endif

#if WITH_CONF_IPSEC_ESP
#define WITH_IPSEC_ESP     WITH_CONF_IPSEC_ESP
#else
#define WITH_IPSEC_ESP     0
#endif

#if WITH_CONF_IPSEC_IKE
#define WITH_IPSEC_IKE  1
#else
#define WITH_IPSEC_IKE  0
#endif

#define WITH_IPSEC    (WITH_IPSEC_ESP | WITH_IPSEC_AH)


#define IPSEC_KEYSIZE_FIXTHIS   16  // Old bad code. Make the key size dynamic.
/*
#define IPSEC_IVSIZE    8
*/

/**
 * IPsec / IKEv2 debug configuration options are set here!
 *
 * There are more debuging options in uip6.c
 * 0: OFF
 * 1: warning & errors
 * 2: full
 */
#define IPSEC_DEBUG 0

#if IPSEC_DEBUG == 2
void memprint(uint8_t *ptr, const uint16_t len);

#define IPSEC_PRINTF(...)         printf("IPsec: " __VA_ARGS__)
#define IPSEC_PRINTF_ERROR(...)   printf("IPsec: *  ERROR  * " __VA_ARGS__)
#define IPSEC_PRINTF_WARNING(...) printf("IPsec: * warning * " __VA_ARGS__)
#define IKE_PRINTF(...)           printf("IKEv2: " __VA_ARGS__)
#define IKE_PRINTF_ERROR(...)     printf("IKEv2: *  ERROR  * " __VA_ARGS__)
#define IPSEC_MEMPRINT(...)       memprint(__VA_ARGS__)

#define IPSEC_MEMPRINTF(str, ptr, len)  do {                                  \
                                      IPSEC_PRINTF(str  " (len %u):\n", len); \
                                      memprint((uint8_t*)ptr, len);           \
                                  } while(0)

#define IPSEC_PRINT6ADDR(addr)    printf("%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x\n", \
                                      ((uint8_t *)addr)[0], ((uint8_t *)addr)[1],    \
                                      ((uint8_t *)addr)[2], ((uint8_t *)addr)[3],    \
                                      ((uint8_t *)addr)[4], ((uint8_t *)addr)[5],    \
                                      ((uint8_t *)addr)[6], ((uint8_t *)addr)[7],    \
                                      ((uint8_t *)addr)[8], ((uint8_t *)addr)[9],    \
                                      ((uint8_t *)addr)[10], ((uint8_t *)addr)[11],  \
                                      ((uint8_t *)addr)[12], ((uint8_t *)addr)[13],  \
                                      ((uint8_t *)addr)[14], ((uint8_t *)addr)[15])

// Prints the contents of an ipsec_addr_t variable at the given address
#define IPSEC_PRINTADDR(addr)     do {                                                                   \
                                      IPSEC_PRINTF("IPSEC ADDR:\n");                                     \
                                      IPSEC_PRINTF(" * Address: ");                                      \
                                      IPSEC_PRINT6ADDR((addr)->peer_addr);                               \
                                      IPSEC_PRINTF(" * Nextlayer proto: %u\n", (addr)->nextlayer_proto); \
                                      IPSEC_PRINTF(" * My port: %u\n", (addr)->my_port);                 \
                                      IPSEC_PRINTF(" * Peer port: %u\n", (addr)->peer_port);             \
                                      IPSEC_PRINTF("END ADDR\n");                                        \
                                  } while(0)

// Prints the contents of an ipsec_addr_set_t located at the given address
#define IPSEC_PRINTADDRSET(addr_set) do {                                                           \
    IPSEC_PRINTF("ADDRSET:\n");                                                                     \
    IPSEC_PRINTF(" * Address from: ");                                                              \
    IPSEC_PRINT6ADDR((addr_set)->peer_addr_from);                                                   \
    IPSEC_PRINTF(" * Address to: ");                                                                \
    IPSEC_PRINT6ADDR((addr_set)->peer_addr_to);                                                     \
    IPSEC_PRINTF(" * Nextlayer proto: %u\n", (addr_set)->nextlayer_proto);                          \
    IPSEC_PRINTF(" * My ports: %u - %u\n", (addr_set)->my_port_from, (addr_set)->my_port_to);       \
    IPSEC_PRINTF(" * Peer ports: %u - %u\n", (addr_set)->peer_port_from, (addr_set)->peer_port_to); \
    IPSEC_PRINTF("END ADDRSET\n");                                                                  \
  } while(0)

#define IPSEC_PRINTLLADDR(lladdr) IPSEC_PRINTF(" %02x:%02x:%02x:%02x:%02x:%02x ", \
                                      lladdr->addr[0], lladdr->addr[1],           \
                                      lladdr->addr[2], lladdr->addr[3],           \
                                      lladdr->addr[4], lladdr->addr[5])

#define IPSEC_MEM_STATS           1
#define IPSEC_TIME_STATS          1

// Prints the SPD entry located at address spd_entry
#define IPSEC_PRINTSPDENTRY(spd_entry) do {                            \
                                      IPSEC_PRINTF("Selector: ");      \
                                      uint8_t str[3][8] = {            \
                                          { "PROTECT" },               \
                                          { "BYPASS" },                \
                                          { "DISCARD" }                \
                                      };                               \
                                      IPSEC_PRINTF("Action: %s\n", str[(spd_entry)->proc_action]); \
                                      IPSEC_PRINTF("Offer at addr: %p\n", (spd_entry)->offer);     \
                                  } while(0)

#define IPSEC_PRINTSPDLOOKUPADDR(addr) do {                                        \
                                      IPSEC_PRINTF("SPD lookup for traffic:\n");   \
                                      IPSEC_PRINTADDR(addr);                       \
                                  } while(0)

#define IPSEC_PRINTFOUNDSPDENTRY(spd_entry) do {                                   \
                                      IPSEC_PRINTF("Found SPD entry:\n");          \
                                      IPSEC_PRINTSPDENTRY(spd_entry);              \
                                  } while(0)                                       \

// Prints the SAD entry located at entry
#define IPSEC_PRINTSADENTRY(entry)                                                                       \
  do {                                                                                                   \
    IPSEC_PRINTF("SAD ENTRY:\n");                                                                        \
    IPSEC_PRINTADDRSET(&(entry)->traffic_desc);                                                          \
    IPSEC_PRINTF(" * SPI: %lu\n", (entry)->spi);                                                         \
    IPSEC_PRINTF(" * Sequence number: %lu\n", (entry)->seqno);                                           \
    IPSEC_PRINTF(" * Window: 0x%lx\n", (entry)->win);                                                    \
    IPSEC_PRINTF(" * Time of creation: %lu\n", (entry)->time_of_creation);                               \
    IPSEC_PRINTF(" * Bytes transported: %lu\n", (entry)->bytes_transported);                             \
    IPSEC_PRINTF(" * SA proto: %u\n", (entry)->sa.proto);                                                \
    IPSEC_PRINTF(" * Encr type: %u\n", (entry)->sa.encr);                                                \
    IPSEC_MEMPRINTF(" * Encr keymat", (entry)->sa.sk_e,  SA_ENCR_KEYMATLEN_BY_SA((entry)->sa));          \
    IPSEC_PRINTF(" * Integ type: %u\n", (entry)->sa.integ);                                              \
    IPSEC_MEMPRINTF(" * Integ keymat", (entry)->sa.sk_a,  SA_INTEG_KEYMATLEN_BY_TYPE((entry)->sa.integ));\
    IPSEC_PRINTF("END SAD ENTRY\n");                                                                    \
  } while(0)

#elif IPSEC_DEBUG == 1

#define IPSEC_PRINTF_ERROR(...)   printf("IPsec: *  ERROR  * " __VA_ARGS__)
#define IPSEC_PRINTF_WARNING(...) printf("IPsec: * warning * " __VA_ARGS__)
#define IKE_PRINTF_ERROR(...)     printf("IKEv2: *  ERROR  * " __VA_ARGS__)
#define IPSEC_PRINTF(...)
#define IKE_PRINTF(...)
#define IPSEC_MEMPRINT(...)
#define IPSEC_MEMPRINTF(...)
#define IPSEC_PRINT6ADDR(addr)
#define IPSEC_PRINTADDR(...)
#define IPSEC_PRINTADDRSET(...)
#define IPSEC_PRINTLLADDR(addr)
#define IPSEC_MEM_STATS 0
#define IPSEC_TIME_STATS 0
#define IPSEC_PRINTSPDLOOKUPADDR(...)
#define IPSEC_PRINTFOUNDSPDENTRY(...)
#define IPSEC_PRINTSPDENTRY(...)
#define IPSEC_PRINTSADENTRY(...)

#elif IPSEC_DEBUG == 0

#define IPSEC_PRINTF(...)
#define IPSEC_PRINTF_WARNING(...)
#define IPSEC_PRINTF_ERROR(...)
#define IKE_PRINTF(...)
#define IKE_PRINTF_ERROR(...)
#define IPSEC_MEMPRINT(...)
#define IPSEC_MEMPRINTF(...)
#define IPSEC_PRINT6ADDR(addr)
#define IPSEC_PRINTADDR(...)
#define IPSEC_PRINTADDRSET(...)
#define IPSEC_PRINTLLADDR(addr)
#define IPSEC_MEM_STATS 0
#define IPSEC_TIME_STATS 0
#define IPSEC_PRINTSPDLOOKUPADDR(...)
#define IPSEC_PRINTFOUNDSPDENTRY(...)
#define IPSEC_PRINTSPDENTRY(...)
#define IPSEC_PRINTSADENTRY(...)

#endif // IPSEC_DEBUG

/* End debug configuration options */

/**
  * The length (in bytes) of the ICV field in the ESP header and that of IKEv2's SK payload.
  *
  * The length of this field is in fact dependent upon the integrity transform, but as most IKEv2 / IPsec
  * transforms uses the below length I figure that it's safe to make it static.
  */
#define IPSEC_ICVLEN   12

#define UIP_PROTO_ESP   50
#define UIP_PROTO_AH    51

#define UINT24_TO_UINT32(uint24) (uint32_t)((uint24[2] << 16) + (uint24[1] << 8 ) + (uint24[0]));

#define UINT32_TO_UINT24(uint32, uint24)        \
        uint24[0] = (uint8_t)(uint32);          \
        uint24[1] = (uint8_t)(uint32>>8);       \
        uint24[2] = (uint8_t)(uint32>>16);

#define UIP_ESP_BUF ((struct uip_esp_header *)&uip_buf[uip_l2_l3_hdr_len])
#define UIP_ESP_BUF_HDR(jump) ((struct uip_esp_header *)&uip_buf[uip_l2_l3_hdr_len+jump])

#define ESP_HDR_SIZE(diet_esp) diet_esp->spi_size + diet_esp->sn_size

#define MIN_OF(a,b) a > b ? b : a

#define CALCULATE_ICV_TRUNCATION(truncation, diet_esp_truncation) 				\
  /* as long as there is ICV longer than 255 byte , that is fine :-) */ \
  truncation = UINT8_MAX;         \
  switch (diet_esp_truncation) {  \
    case 1:                       \
      truncation = 1;             \
      break;                      \
    case 2:                       \
      truncation = 2;             \
      break;                      \
    case 3:                       \
      truncation = 4;             \
      break;                      \
    case 4:                       \
      truncation = 8;             \
      break;                      \
    default:                      \
      break;                      \
}

union header_field_size_t {
  uint8_t  size_0[0];
  uint8_t  size_8;
  uint16_t size_16;
  uint8_t  size_24[3];
  uint32_t size_32;
};

/* ESP header as defined in RFC 2406 */
struct uip_esp_header {
  uint32_t          spi;
  uint32_t          seqno;
  /**
    * IV and the data will now follow. These are both of variable length.

  unsigned char     iv[IPSEC_IVSIZE];
  unsigned char     data[0];
  */
};

/* The length of extension headers data coming after the payload */
extern uint8_t uip_ext_end_len;

static inline int uip_str2ip6(const char* str, uip_ip6addr_t *host) {
    int i;
    for (i=0; i<16; i++) host->u8[i] = str[i];
    return 0;
}

static int is_my_address(const char* addr) {
    int i, j;
    uint8_t state;

    for(i = 0; i < UIP_DS6_ADDR_NB; i++) {
        state = uip_ds6_if.addr_list[i].state;
        if(uip_ds6_if.addr_list[i].isused &&
               (state == ADDR_TENTATIVE || state == ADDR_PREFERRED)) {
            for (j = 0; j<16; j++) {
                if (addr[j] != uip_ds6_if.addr_list[i].ipaddr.u8[j])
                    break;
            }
            if (j == 16)
                return 1;
        }
    }
    return 0;
}

#endif /* __IPSEC_H__ */
/** @} */
